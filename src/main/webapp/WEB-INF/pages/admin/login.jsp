<%--
  Created by IntelliJ IDEA.
  User: phaolo
  Date: 2020-07-08
  Time: 18:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" isELIgnored="false"%>
<%@ taglib prefix="m" uri="http://molu.vn/template" %>

<%@ include file="/common/taglibs.jsp"%>
<m:adl>
  <jsp:attribute name="content">
    <div class="m-t-10 p-20">

      <!-- message -->
      <div class="row">
        <div class="col-xs-12 text-xs-center">
          <h6 style="color: red;">${(msg != null)? msg: ''}</h6>
        </div>
      </div>

      <form class="m-t-20" action="/admin/login.html" method="post">

        <div class="form-group row">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Username" name="username">
          </div>
        </div>

        <div class="form-group row">
          <div class="col-xs-12">
            <input class="form-control" type="password" required="" placeholder="Password" name="password">
          </div>
        </div>

        <div class="form-group row">
          <div class="col-xs-12">
            <div class="checkbox checkbox-custom">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup"><fmt:message key="label.remember_me"/> </label>
            </div>
          </div>
        </div>

        <div class="form-group text-center row m-t-10">
          <div class="col-xs-12">
            <button class="btn btn-success btn-block waves-effect waves-light" type="submit"><fmt:message key="label.log_in"/> </button>
          </div>
        </div>

        <div class="form-group row m-t-30 m-b-0">
          <div class="col-sm-12">
            <a href="pages-recoverpw.html" class="text-muted"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
          </div>
        </div>

      </form>
    </div>
  </jsp:attribute>
</m:adl>


