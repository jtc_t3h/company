<%--
  Created by IntelliJ IDEA.
  User: phaolo
  Date: 2020-07-05
  Time: 15:24
  To change this template use File | Settings | File Templates.
--%>
<%@ tag language="java" pageEncoding="UTF-8" %>
<%@ attribute name="content" required="true" fragment="true" %>

<%@ include file="/common/taglibs.jsp" %>

<!DOCTYPE html>
<html>

<!-- Mirrored from coderthemes.com/uplon_1.4/horizontal/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 01 Dec 2016 10:13:49 GMT -->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
  <meta name="author" content="Coderthemes">

  <!-- App Favicon -->
  <link rel="shortcut icon" href="<c:url value='/themes/admin/images/favicon.ico'/>">

  <!-- App title -->
  <title>Uplon - Responsive Admin Dashboard Template</title>

  <!-- App CSS -->
  <link href="<c:url value='/themes/admin/css/style.css'/>" rel="stylesheet" type="text/css"/>

  <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->

  <script src="<c:url value='/themes/admin/js/modernizr.min.js'/>"></script>
  <script>
      (function (i, s, o, g, r, a, m) {
          i['GoogleAnalyticsObject'] = r;
          i[r] = i[r] || function () {
              (i[r].q = i[r].q || []).push(arguments)
          }, i[r].l = 1 * new Date();
          a = s.createElement(o),
              m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          m.parentNode.insertBefore(a, m)
      })(window, document, 'script', '../../../www.google-analytics.com/analytics.js', 'ga');

      ga('create', 'UA-79190402-1', 'auto');
      ga('send', 'pageview');

  </script>

</head>


<body>

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">

  <div class="account-bg">
    <div class="card-box m-b-0">
      <div class="text-xs-center m-t-20">
        <a href="index.html" class="logo">
          <i class="zmdi zmdi-group-work icon-c-logo"></i>
          <span><fmt:message key="site.name"/> </span>
        </a>
      </div>

      <jsp:invoke fragment="content"/>

      <div class="clearfix"></div>
    </div>
  </div>
  <!-- end card-box-->

  <div class="m-t-20">
    <div class="text-xs-center">
      <p class="text-white"><fmt:message key="label.not_account"/>
        <a href="pages-register.html" class="text-white m-l-5"><b><fmt:message key="lable.register"/> </b></a>
      </p>
    </div>
  </div>

</div>
<!-- end wrapper page -->


<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<!-- jQuery  -->
<script src="<c:url value='/themes/admin/js/jquery.min.js'/>"></script>
<script src="<c:url value='/themes/admin/js/tether.min.js'/>"></script><!-- Tether for Bootstrap -->
<script src="<c:url value='/themes/admin/js/bootstrap.min.js'/>"></script>
<script src="<c:url value='/themes/admin/js/waves.js'/>"></script>
<script src="<c:url value='/themes/admin/js/jquery.nicescroll.js'/>"></script>
<script src="<c:url value='/themes/admin/plugins/switchery/switchery.min.js'/>"></script>

<!-- App js -->
<script src="<c:url value='/themes/admin/js/jquery.core.js'/>"></script>
<script src="<c:url value='/themes/admin/js/jquery.app.js'/>"></script>

</body>

<!-- Mirrored from coderthemes.com/uplon_1.4/horizontal/pages-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 01 Dec 2016 10:13:49 GMT -->
</html>
