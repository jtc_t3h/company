<%--
  Created by IntelliJ IDEA.
  User: phaolo
  Date: 2020-07-05
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="/common/taglibs.jsp"%>
<footer class="footer text-right">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <fmt:message key="site.copyright"/>
      </div>
    </div>
  </div>
</footer>