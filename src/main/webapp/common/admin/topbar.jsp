<%--
  Created by IntelliJ IDEA.
  User: phaolo
  Date: 2020-07-05
  Time: 15:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ include file="/common/taglibs.jsp"%>
<div class="topbar-main">
  <div class="container">

    <!-- LOGO -->
    <div class="topbar-left">
      <a href="index.html" class="logo">
        <i class="zmdi zmdi-group-work icon-c-logo"></i>
        <span>Uplon</span>
      </a>
    </div>
    <!-- End Logo container-->


    <div class="menu-extras">

      <ul class="nav navbar-nav pull-right">

        <li class="nav-item">
          <!-- Mobile menu toggle-->
          <a class="navbar-toggle">
            <div class="lines">
              <span></span>
              <span></span>
              <span></span>
            </div>
          </a>
          <!-- End mobile menu toggle-->
        </li>


        <li class="nav-item dropdown notification-list">
          <a class="nav-link dropdown-toggle arrow-none waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
             aria-haspopup="false" aria-expanded="false">
            <img src="/themes/admin/images/users/avatar-1.jpg" alt="user" class="img-circle">
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-arrow profile-dropdown " aria-labelledby="Preview">
            <!-- item-->
            <div class="dropdown-item noti-title">
              <h5 class="text-overflow"><small><fmt:message key="label.welcome"/> ! ${USER_SESSION.fullName}</small> </h5>
            </div>

            <!-- item-->
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <i class="zmdi zmdi-account-circle"></i> <span>Profile</span>
            </a>

            <!-- item-->
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <i class="zmdi zmdi-settings"></i> <span>Settings</span>
            </a>

            <!-- item-->
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <i class="zmdi zmdi-lock-open"></i> <span>Lock Screen</span>
            </a>

            <!-- item-->
            <a href="javascript:void(0);" class="dropdown-item notify-item">
              <i class="zmdi zmdi-power"></i> <span>Logout</span>
            </a>

          </div>
        </li>

      </ul>

    </div> <!-- end menu-extras -->
    <div class="clearfix"></div>

  </div> <!-- end container -->
</div>
