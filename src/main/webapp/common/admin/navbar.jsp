<%--
  Created by IntelliJ IDEA.
  User: phaolo
  Date: 2020-07-05
  Time: 15:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/common/taglibs.jsp"%>
<div class="navbar-custom">
  <div class="container">
    <div id="navigation">
      <!-- Navigation Menu-->
      <ul class="navigation-menu">
        <li>
          <a href="index.html"><i class="zmdi zmdi-view-dashboard"></i> <span> <fmt:message key="sidebar.dashboard"/> </span> </a>
        </li>

        <li class="has-submenu">
          <a href="#"><i class="zmdi zmdi-album"></i> <span> <fmt:message key="sidebar.menu.system"/> </span> </a>
          <ul class="submenu">
            <li><a href="icons-materialdesign.html"><fmt:message key="sidebar.user"/> </a></li>
          </ul>
        </li>

      </ul>
      <!-- End navigation menu  -->
    </div>
  </div>
</div>
