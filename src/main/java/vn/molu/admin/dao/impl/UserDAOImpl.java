package vn.molu.admin.dao.impl;

import vn.molu.admin.dao.UserDAO;
import vn.molu.admin.domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 * @author phaolo
 * @since 2020-07-08
 */
public class UserDAOImpl implements UserDAO {

    @Override
    public User findByUserName(String username) {


        User user = new User();
        user.setUsername(username);
        user.setPassword("123456");
        user.setFullName("Phaolo Pham");

        List<String> listOfRoles = new ArrayList<>();
        listOfRoles.add("/admin/index.html");
        listOfRoles.add("/admin/user.html");
        user.setListOfRoles(listOfRoles);

        return user;
    }
}
