package vn.molu.admin.dao;

import vn.molu.admin.domain.User;

/**
 * @author phaolo
 * @since 2020-07-08
 */
public interface UserDAO {

    public User findByUserName(String username);
}
