package vn.molu.admin.controller;

import vn.molu.admin.dao.UserDAO;
import vn.molu.admin.dao.impl.UserDAOImpl;
import vn.molu.admin.domain.User;
import vn.molu.utils.Constant;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author phaolo
 * @since 2020-07-08
 */
@WebServlet("/admin/login.html")
public class LoginController extends HttpServlet {

    private UserDAO userDAO = new UserDAOImpl();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        //
        User user = userDAO.findByUserName(username);

        //
        if (password.equals(user.getPassword())){
            HttpSession session = request.getSession(true);
            session.setAttribute(Constant.USER_SESSION, user);

            response.sendRedirect("/admin/index.html");
        } else {
            request.setAttribute("msg", "Usernam or password incorrect.");
            request.getRequestDispatcher("/WEB-INF/pages/admin/login.jsp").forward(request, response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/pages/admin/login.jsp").forward(request, response);
    }
}
