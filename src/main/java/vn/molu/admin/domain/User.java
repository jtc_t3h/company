package vn.molu.admin.domain;

import java.util.List;

/**
 * @author phaolo
 * @since 2020-07-08
 */
public class User {

    private Long id;
    private String username;
    private String password;
    private String fullName;

    private List<String> listOfRoles;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public List<String> getListOfRoles() {
        return listOfRoles;
    }

    public void setListOfRoles(List<String> listOfRoles) {
        this.listOfRoles = listOfRoles;
    }
}
