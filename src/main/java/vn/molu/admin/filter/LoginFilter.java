package vn.molu.admin.filter;

import vn.molu.admin.domain.User;
import vn.molu.utils.Constant;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * @author phaolo
 * @since 2020-07-08
 */
@WebFilter(filterName = "LoginFilter", value = "/admin/*")
public class LoginFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        //
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        //
        HttpSession session = request.getSession(false);

        //
        String uri = request.getRequestURI();
        User user = (session != null)? (User)session.getAttribute(Constant.USER_SESSION): null;

        if ( uri.endsWith("login.html") || (user != null && user.getListOfRoles().contains(uri))){
            chain.doFilter(req, resp);
        } else {
            response.sendRedirect("/admin/login.html");
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
